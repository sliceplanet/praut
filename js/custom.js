
$(function() {
  /*location*/
  if ($('#map').length) {


    function initializeMap() {
      var mapOptions = {
        zoom: 11,
        scrollwheel: true,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        disableDefaultUI: true,
        draggable: true,
        center: new google.maps.LatLng(55.7530932, 37.606437),
        styles: [{
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [{
              "saturation": "-100"
            }]
          },
          {
            "featureType": "administrative.province",
            "elementType": "all",
            "stylers": [{
              "visibility": "off"
            }]
          },
          {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
              },
              {
                "lightness": 65
              },
              {
                "visibility": "on"
              }
            ]
          },
          {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
              },
              {
                "lightness": "50"
              },
              {
                "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
              "saturation": "-100"
            }]
          },
          {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
              "visibility": "simplified"
            }]
          },
          {
            "featureType": "road.arterial",
            "elementType": "all",
            "stylers": [{
              "lightness": "30"
            }]
          },
          {
            "featureType": "road.local",
            "elementType": "all",
            "stylers": [{
              "lightness": "40"
            }]
          },
          {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
              },
              {
                "visibility": "simplified"
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{
                "hue": "#ffff00"
              },
              {
                "lightness": -25
              },
              {
                "saturation": -97
              }
            ]
          },
          {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [{
                "lightness": -25
              },
              {
                "saturation": -100
              }
            ]
          }
        ]
      }
      var map = new google.maps.Map(document.getElementById('map'), mapOptions);
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(55.7530932, 37.606437),
        map: map,
        title: 'Праут',
        icon: 'images/map-marker.png'
      });
    }

    google.maps.event.addDomListener(window, 'load', initializeMap);
  }





  if ($('#map1').length) {

    function initializeMap() {
      var mapOptions = {
        zoom: 13,
        scrollwheel: true,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        disableDefaultUI: true,
        draggable: true,
        center: new google.maps.LatLng(55.752929, 37.60291),
        styles: [
    {
        "featureType": "landscape",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 60
            }
        ]
    },
    {
        "featureType": "road.local",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 40
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 30
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ef8c25"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#b6c54c"
            },
            {
                "lightness": 40
            },
            {
                "saturation": -40
            }
        ]
    }
    
]
      }
      var map = new google.maps.Map(document.getElementById('map1'), mapOptions);
      var locations = [
        ['Office 4', 55.77378, 37.603625, 4],
        ['Office 3', 55.734679, 37.642532, 3],
        ['Office 2', 55.761653, 37.658572, 2],
        ['Office 1', 55.752929, 37.60291, 1]
      ];
      var marker, i;

      for (i = 0; i < locations.length; i++) {
        var place = locations[i];        
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map,
          icon: 'images/map-marker-no-logo.png',
          zIndex: place[3]
        });

        google.maps.event.addListener(marker, 'click', function () {           
            $('.office-tab-items').find('a[href="#'+this.zIndex+'a"]').trigger('click');
            $('html,body').stop(true, false).animate({scrollTop: $('html,body').offset().top+100}, 800)
                    
        });
      }
      google.maps.event.addDomListener(window, "resize", function() {
         var center = map.getCenter();
         google.maps.event.trigger(map, "resize");
         map.setCenter(center); 
        });

    }
    google.maps.event.addDomListener(window, 'load', initializeMap);
  }



  if ($('#map2').length) {

    function initializeMap() {
      var mapOptions = {
        zoom: 15,
        scrollwheel: true,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        disableDefaultUI: true,
        draggable: true,
        center: new google.maps.LatLng(55.752929, 37.60291),
        styles: [
    {
        "featureType": "landscape",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 60
            }
        ]
    },
    {
        "featureType": "road.local",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 40
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 30
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ef8c25"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#b6c54c"
            },
            {
                "lightness": 40
            },
            {
                "saturation": -40
            }
        ]
    }
    
]
      }
      var map2 = new google.maps.Map(document.getElementById('map2'), mapOptions);
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(55.752929, 37.60291),
        map: map2,
        title: 'Праут',
        icon: 'images/map-marker-no-logo.png'
      });

      $('a[href="#1a"]').on('shown.bs.tab', function() {

        var map = map2,
        center = map2.getCenter();
        google.maps.event.trigger(map2, 'resize');
        map2.setCenter(center);
        $('html,body').animate({scrollTop: $('html,body').offset().top+100}, 800);
      });
    }
    google.maps.event.addDomListener(window, 'load', initializeMap);
  }

  if ($('#map3').length) {

    function initializeMap() {
      var mapOptions = {
        zoom: 15,
        scrollwheel: true,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        disableDefaultUI: true,
        draggable: true,
        center: new google.maps.LatLng(55.761653, 37.658572),
        styles: [
    {
        "featureType": "landscape",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 60
            }
        ]
    },
    {
        "featureType": "road.local",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 40
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 30
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ef8c25"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#b6c54c"
            },
            {
                "lightness": 40
            },
            {
                "saturation": -40
            }
        ]
    }
    
]
      }
      var map3 = new google.maps.Map(document.getElementById('map3'), mapOptions);
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(55.761653, 37.658572),
        map: map3,
        title: 'Праут',
        icon: 'images/map-marker-no-logo.png'
      });

      $('a[href="#2a"]').on('shown.bs.tab', function() {

        var map = map3,
          center = map3.getCenter();
        google.maps.event.trigger(map3, 'resize');
        map3.setCenter(center);
        $('html,body').animate({scrollTop: $('html,body').offset().top+100}, 800);
      });
    }
    google.maps.event.addDomListener(window, 'load', initializeMap);
  }

  if ($('#map4').length) {

    function initializeMap() {
      var mapOptions = {
        zoom: 15,
        scrollwheel: true,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        disableDefaultUI: true,
        draggable: true,
        center: new google.maps.LatLng(55.734679, 37.642532),
        styles: [
    {
        "featureType": "landscape",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 60
            }
        ]
    },
    {
        "featureType": "road.local",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 40
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 30
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ef8c25"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#b6c54c"
            },
            {
                "lightness": 40
            },
            {
                "saturation": -40
            }
        ]
    }
    
]
      }
      var map4 = new google.maps.Map(document.getElementById('map4'), mapOptions);
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(55.734679, 37.642532),
        map: map4,
        title: 'Праут',
        icon: 'images/map-marker-no-logo.png'
      });

      $('a[href="#3a"]').on('shown.bs.tab', function() {

        var map = map4,
          center = map4.getCenter();
        google.maps.event.trigger(map4, 'resize');
        map4.setCenter(center);
        $('html,body').animate({scrollTop: $('html,body').offset().top+100}, 800);
      });
    }
    google.maps.event.addDomListener(window, 'load', initializeMap);
  }

  if ($('#map5').length) {

    function initializeMap() {
      var mapOptions = {
        zoom: 15,
        scrollwheel: true,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        disableDefaultUI: true,
        draggable: true,
        center: new google.maps.LatLng(55.77378, 37.603625),
        styles: [
    {
        "featureType": "landscape",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 60
            }
        ]
    },
    {
        "featureType": "road.local",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 40
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "transit",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 30
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ef8c25"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#b6c54c"
            },
            {
                "lightness": 40
            },
            {
                "saturation": -40
            }
        ]
    }
    
]
      }
      var map5 = new google.maps.Map(document.getElementById('map5'), mapOptions);
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(55.77378, 37.603625),
        map: map5,
        title: 'Праут',
        icon: 'images/map-marker-no-logo.png'
      });

      $('a[href="#4a"]').on('shown.bs.tab', function() {

        var map = map5,
          center = map5.getCenter();
        google.maps.event.trigger(map5, 'resize');
        map5.setCenter(center);
      });
      $('a[href="#4a"]').on('click', function() {

        var map = map5,
          center = map5.getCenter();
        google.maps.event.trigger(map5, 'resize');
        map5.setCenter(center);
        $('html,body').animate({scrollTop: $('html,body').offset().top+100}, 800);
      });
    }
    google.maps.event.addDomListener(window, 'load', initializeMap);
  }
    /* formstyler */
    if ($('.js-styled').length) {
        $('.js-styled').styler();
    }

    /* office items link */
    $('.office-tab-items__link').click(function(e) {
        e.preventDefault();
        $('.office-tab-items__link.active').removeClass('active');
        $(this).addClass('active');        
    });
    $('.js-office-all__link').click(function(e) {
        e.preventDefault();
        $('.tab-pane.active').removeClass('active');
        $('#0a').addClass('active');        
        $('.office-tab-items__link.active').removeClass('active'); 
    });

    /* clean calculator inputs value */
    $( ".js-calc-1 .form-control" ).each(function() {
        $(this).focus(function() {
            var input = $(".js-calc-1 .form-control");
            if (input.value == input.value) {
                input.attr("placeholder", "0");
            }
        }).blur(function() {
            var input = $(".js-calc-1 .form-control");
            if (input.value == "0") {               
                input.attr("placeholder",input.value);
            }
        });
    });
    $( ".js-calc-2 .form-control" ).each(function() {
        $(this).focus(function() {
            var input = $(".js-calc-2 .form-control");
            if (input.value == input.value) {
                input.attr("placeholder", "0");
            }
        }).blur(function() {
            var input = $(".js-calc-2 .form-control");
            if (input.value == "0") {               
                input.attr("placeholder",input.value);
            }
        });
    });

    var submitIcon = $('.searchbox-icon');
    var inputBox = $('.searchbox-input');
    var searchBox = $('.searchbox');
    var phone = $('.js-phone');
    var isOpen = false;
    submitIcon.click(function() {
      if (isOpen == false) {
        searchBox.addClass('searchbox-open');
        phone.css('margin-left', '-120px');
        inputBox.focus();
        isOpen = true;
      } else {
        searchBox.removeClass('searchbox-open');
        phone.css('margin-left', '0px');
        inputBox.focusout();
        isOpen = false;
      }
    });
    submitIcon.mouseup(function() {
      return false;
    });
    searchBox.mouseup(function() {
      return false;
    });
    $(document).mouseup(function() {
      if (isOpen == true) {
        $('.searchbox-icon').css('display', 'block');
        submitIcon.click();
      }
    });




  function buttonUp() {
    var inputVal = $('.searchbox-input').val();
    inputVal = $.trim(inputVal).length;
    if (inputVal !== 0) {
      $('.searchbox-icon').css('display', 'none');
    } else {
      $('.searchbox-input').val('');
      $('.searchbox-icon').css('display', 'block');
    }
  }

  $('.outsourcing-services__list').on('click', 'li:not(.outsourcing-services__item_active)', function() {
    if ($(window).width() > 767) {
      $(this)
        .addClass('outsourcing-services__item_active')
        .siblings()
        .removeClass('outsourcing-services__item_active');
      $('.outsourcing-tab__item')
        .fadeOut('800')
        .eq($(this).index())
        .fadeIn('800');
    }
  });



  

  

  $('.popup-adv__brief-btn').click(function() {
    if ($(this).hasClass('popup-adv__brief-btn_active')) {
      $('.popup-adv__brief-div').css('height', '0');
      $(this).removeClass('popup-adv__brief-btn_active');
    } else {
      $('.popup-adv__brief-div').css('height', '100%');
      $(this).addClass('popup-adv__brief-btn_active');
    }
  });


  var wrapper = $(".popup-adv__fileform"),
    inp = wrapper.find("input"),
    btn = wrapper.find("button"),
    lbl = wrapper.find("div");
  var file_api = (window.File && window.FileReader && window.FileList && window.Blob) ? true : false;
  inp.change(function() {
    var file_name;
    if (file_api && inp[0].files[0]) file_name = inp[0].files[0].name;
    else
      file_name = inp.val().replace("C:\\fakepath\\", '');
    if (!file_name.length) return;
    if (lbl.is(":visible")) {
      lbl.text(file_name);
      // btn.text("Загрузить файл");
    } else
      btn.text(file_name);
  }).change();

});
$(window).load(function() {
/*Tabs*/
/*$('.js-tab-link').click(function(){
        $(this).parents('.js-tab-wrap').find('.js-tab-cont').addClass('hide-tab');
        $(this).parent().siblings().removeClass('active');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        $(this).parent().addClass('active');
   return false;
  });*/

//Accordion

   $('.js-accordion-item').on('click', accordion);

  function accordion() {

    if ($(this).is('[data-accordion="true"]')) {
      accordionClose(this);
    } else {
      accordionCloseAll();
      accordionOpen(this);
    }
    return false;
  }

  function isOpen(self) {
    return $(self).is('[data-accordion="true"]');
  }

  


/* viewport width */
   

});
function accordionOpen(self) {
    $(self).addClass('active');
    $(self).attr('data-accordion', 'true');
    $(self).next('.js-accordion-item__content').slideDown();
    $(self).children('.fa-angle-right').css('transform', 'rotate(270deg)');
  }

  function accordionClose(self) {
    $(self).removeClass('active');
    $(self).attr('data-accordion', 'false');
    $(self).next('.js-accordion-item__content').slideUp();
    $(self).children('.fa-angle-right').css('transform', 'rotate(90deg)');
  }

  function accordionCloseAll() {
    $('.js-accordion-item').each(function() {
      if ($(this).is('[data-accordion="true"]')) {
        accordionClose(this);
      }
    });
  }
  function accordionOpenAll() {
    $('.js-accordion-item').each(function() {
      if ($(this).is('[data-accordion="false"]')) {
        accordionOpen(this);
      }
    });
  }
 function viewport(){
        var e = window, 
            a = 'inner';
        if ( !( 'innerWidth' in window ) )
        {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
    };

var handler = function(){
    
    if(viewport().width < 768)
    {
        accordionCloseAll();
    }
    else
    {
        accordionOpenAll()
    }

}
$(window).bind('load', handler);
$(window).bind('resize', handler);