"use strict";

var gulp       = require('gulp'),
    sass       = require('gulp-sass'),
    connect    = require('gulp-connect'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    htmlmin = require('gulp-htmlmin'),
    zip = require('gulp-zip'),
    minify = require('gulp-minify');

//Livereload
gulp.task('connect', function() {
  connect.server({
    root: './',
    port: 8080,
    livereload: true
  });
});

// SASS
// gulp.task('sassbuild', function () {
//   return gulp.src(['scss/**/*.sass', 'scss/**/*.scss'])
//     .pipe(sass({outputStyle: 'expanded'}).on('error',sass.logError))
//     .pipe(autoprefixer({browsers: ['last 2 versions','> 1%','ie 9']}))
//     .pipe(csso())
//     .pipe(connect.reload());
// });

gulp.task('sass', function () {
  return gulp.src(['scss/**/*.sass', 'scss/**/*.scss'])
    .pipe(sass({outputStyle: 'expanded'}).on('error',sass.logError))
    .pipe(autoprefixer({browsers: ['last 2 versions','> 1%','ie 9']}))
    .pipe(gulp.dest('css'))
    .pipe(connect.reload());
});

// HTML
gulp.task('html', function () {
  return gulp.src('./*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(connect.reload());
});

// Watch for changes is SASS and HTML
gulp.task('watch', function(){
 gulp.watch(['scss/**/*.sass', 'scss/**/*.scss'], ['sass'])
 gulp.watch('./index.html', ['html']);
})

gulp.task('jsminify', function() {
  gulp.src('js/*.js')
    .pipe(minify({
        ext:{
            min:'.js'
        },
        ignoreFiles: ['.combo.js', '-min.js', 'min.js']
    }))
});

// default
gulp.task('default', ['html','sass','watch','connect']);

gulp.task('imagemin', () =>
    gulp.src('img/*')
        .pipe(imagemin())

);

gulp.task('removedist', function() { return del.sync('dist'); });

